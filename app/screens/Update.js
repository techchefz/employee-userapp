import React from 'react';
import { View, ScrollView, TextInput, StyleSheet, Picker, Text, Button, SafeAreaView, StatusBar, KeyboardAvoidingView, Dimensions, Platform, AsyncStorage, TouchableOpacity } from 'react-native';
import { Header, Card, } from 'react-native-elements';
import DatePicker from 'react-native-datepicker'
import IP from '../ip';

export default class Update extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      workObj: [],
      user: null,
      date: null,
      hours: null,
      project: null,
      projects: [],
      day: null,
      activities: '',
    };
  }


  componentWillMount() {
    let date = new Date().getDate();
    let month = new Date().getMonth() + 1;
    let year = new Date().getFullYear();
    AsyncStorage.getItem('uname')
      .then((uname) => {
        let name = uname;
        name = name.replace(/^"(.*)"$/, '$1');
        this.setState({ user: name })
      })
      .catch(() => {
        console.log('Error getting username from AsyncStorage')
      });
    let fullDate = date + '-' + month + '-' + year;
    this.setState({ todayDate: fullDate, day: '1' });
    this.loadProjects();

  }

  async loadProjects() {
    await fetch('http://' + IP + '/get-projects')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          projects: responseJson
        });
      })
      .catch((error) => {
        alert('Error fetching projects');
      });
  }


  async save() {
    let day = parseInt(this.state.day);
    let obj = {
      user: this.state.user,
      date: this.state.date,
      hours: this.state.hours,
      project: this.state.project,
      activities: this.state.activities,
    };
    let workObj = this.state.workObj;
    workObj.push(obj);
    await this.setState({ workObj: workObj });
    await this.setState({
      hours: null,
      project: null,
      day: day + 1,
      activities: ''
    });
    alert('Saved');
  }

  async submit() {
    let saved = this.state.workObj;

    fetch('http://' + IP + '/update-logs', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        saved
      }),
    })
      .then((res) => {
        if (res.status == 200) {
          alert('Uploaded successfully');
          this.props.navigation.navigate('Home');
        } else {
          alert('Error Uploading');
        }
      })
      .catch(() => {
        console.log('Server Not Responding');
      });

    this.setState(
      {
        workObj: [],
        date: null,
        hours: null,
        project: null,
        projects: [],
        day: null,
        submit: false,
        activities: '',
      }
    );
    this.props.navigation.navigate('Home');
  }

  render() {
    return (
      <View>
        <StatusBar
          backgroundColor="blue"
          barStyle="light-content"
        />
        <SafeAreaView style={{ backgroundColor: "#008CD0" }} />
        <Header backgroundColor="#008CD0"
          placement="left"
          leftComponent={{
            icon: 'menu', color: '#fff', fontSize: 18, onPress:
              () => this.props.navigation.toggleDrawer()
          }}
          centerComponent={{ text: 'Update Work', style: { color: '#fff', fontSize: 18 } }}
        />
        <KeyboardAvoidingView behavior="padding" enabled>
          <ScrollView style={{ height: "100%" }}>
            <Card
              containerStyle={{ height: Dimensions.get("screen").height * (2 / 3) }}
              title={this.state.date ?
                ('Day ' + this.state.day + ' ' + this.state.date) : ('Day ' + this.state.day)}>
              <Text>Date</Text>
              <DatePicker
                style={{ width: "100%" }} date={this.state.date} mode="date" placeholder="Select Date" format="YYYY-MM-DD"
                confirmBtnText="Confirm" cancelBtnText="Cancel" customStyles={{
                  dateIcon: { position: 'absolute', left: 0, top: 4, marginLeft: 0 }, dateInput: { marginLeft: 36 }
                }}
                onDateChange={(date) => { this.setState({ date: date }) }}
              />
              <Text style={{ marginTop: 20 }}>Hours</Text>
              <TextInput
                placeholder='Hours Worked'
                keyboardType='numeric'
                onChangeText={(hrs) => this.setState({ hours: hrs })}
                style={styles.input}
              />
              <Text style={{ marginTop: 20, height: 20, }}> Project </Text>
              <Picker
                selectedValue={this.state.project}
                style={{ height: 50, width: "100%", borderColor: "gray", borderBottomWidth: 0.5 }}
                onValueChange={(itemValue, itemIndex) => this.setState({ project: itemValue })}
              >
                {
                  this.state.projects.map((project, i) => {
                    return (<Picker.Item label={project} value={project} key={i + i * 37} />);
                  })
                }
              </Picker>
              <Text style={{ marginTop: 20 }}>
                Activities Performed
          </Text>
              <TextInput style={{ borderColor: "gray", borderWidth: 0.5, padding: 10, height: "20%" }}
                placeholder="Activities Performed"
                multiline={true}
                numberOfLines={4}
                onChangeText={(text) => this.setState({ activities: text })}
                value={this.state.activities}
              />
              <View
                style={{ justifyContent: "space-around", alignItems: "center", flexDirection: "row", width: "100%" }}>
                <TouchableOpacity
                  onPress={() => { this.save() }}
                  style={[styles.Button, {
                    backgroundColor: "#008CD0",
                    marginTop: 30,

                    justifyContent: "center",
                    width: "40%",
                    alignItems: "center",
                    borderRadius: 7,
                    padding: 10,
                    ...Platform.select({
                      ios: {
                        shadowColor: "black",
                        shadowOffset: {
                          width: 2,
                          height: 2
                        },
                        shadowOpacity: 0.5,
                        shadowRadius: 2,
                      },
                      android: { elevation: 3 }
                    })
                  }]}>
                  <Text style={{ color: "#fff" }}>Save</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => { this.submit() }}
                  style={[styles.Button, {
                    backgroundColor: "#008CD0",
                    marginTop: 30,
                    justifyContent: "center",
                    width: "40%",
                    alignItems: "center",
                    borderRadius: 7,
                    padding: 10,
                    ...Platform.select({
                      ios: {
                        shadowColor: "black",
                        shadowOffset: {
                          width: 2,
                          height: 2
                        },
                        shadowOpacity: 0.5,
                        shadowRadius: 2,
                      },
                      android: { elevation: 3 }
                    })
                  }]}>
                  <Text style={{ color: "#fff" }}>Submit</Text>
                </TouchableOpacity>
              </View>
            </Card>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    height: 40,
    color: 'black',
    paddingHorizontal: 10,
    borderBottomWidth: 0.5,
    borderColor: "gray"
  }
});
