import React, { Component } from 'react';
import { StyleSheet, View, TextInput, Button, AsyncStorage, Platform, TouchableOpacity, Text} from 'react-native';
import IP from '../ip';
import Icon from 'react-native-vector-icons/Zocial';
import Micon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            uname: '',
            password: '',
        }
    }

    async checkCredentials() {
        fetch('http://' + IP + '/login', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: this.state.uname.toLowerCase(),
                password: this.state.password.toLowerCase(),
                userType: 'employee',
            }),
        })
            .then((res) => {
                if (res.status == 200) {
                    AsyncStorage.setItem('uname', JSON.stringify(this.state.uname.toLowerCase()))
                        .then(() => { console.log('Uname Saved') })
                        .catch(() => { console.log('Error occured while saving Uname') });
                    this.props.navigation.navigate('Dashboard');
                } else if (res.status == 400) {
                    alert("User Not Registered");
                } else if (res.status == 401) {
                    alert("Incorrect Password")
                }
            });
    }


    render() {
        return (
            <View style={styles.container}>
                <View style={{
                    height: 60,
                    backgroundColor: "white",
                    width: "90%",
                    padding: 5,
                    borderRadius: 7,
                    marginTop: 20,
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                    ...Platform.select({
                        ios: {
                            shadowColor: "black",
                            shadowOffset: {
                                width: 2,
                                height: 2
                            },
                            shadowOpacity: 0.5,
                            shadowRadius: 2,
                        },
                        android: { elevation: 3 }
                    })
                }}>
                    <View style={{ flex: 1 }}>
                        <Icon name='email'
                            color='#808080'
                            size={20}
                        />
                    </View>
                    <TextInput
                        style={{ flex: 9, height: 40 }}
                        placeholder='username'
                        onChangeText={(uname) => this.setState({ uname: uname })}
                    />
                </View>
                <View style={{
                    height: 60,
                    backgroundColor: "white",
                    marginTop: 10, padding: 5,
                    width: "90%",
                    flexDirection: "row",
                    borderRadius: 7,
                    justifyContent: "center",
                    alignItems: "center",
                    ...Platform.select({
                        ios: {
                            shadowColor: "black",
                            shadowOffset: {
                                width: 1,
                                height: 1
                            },
                            shadowOpacity: 1,
                            shadowRadius: 1,
                        },

                        android: { elevation: 2 }
                    })
                }}>
                    <View style={{ flex: 1 }}>
                        <Micon name='lock'
                            color='#808080'
                            size={20}
                        />
                    </View>
                    <TextInput
                        style={{ height: 40, flex: 9 }}
                        placeholder="password"
                        onChangeText={(password) => { this.setState({ password: password }) }}
                        secureTextEntry
                    />
                </View>
                <View style={ {justifyContent:"center",alignItems:"center"}}>
                <TouchableOpacity
                    onPress={() => this.checkCredentials()}
                    style={[styles.Button, {
                        backgroundColor: "#008CD0",
                        marginTop: 30,
                        justifyContent: "center",
                        width: "90%",
                        alignItems: "center",
                        borderRadius: 7,
                        padding: 10,
                        ...Platform.select({
                            ios: {
                                shadowColor: "black",
                                shadowOffset: {
                                    width: 1,
                                    height: 1
                                },
                                shadowOpacity: 1,
                                shadowRadius: 1,
                            },

                            android: { elevation: 2 }
                        })
                    }]}>
                    <Text style={{ color: "#fff" }}>Login</Text>
                </TouchableOpacity>
                </View>
                <View style={{
                    width: "90%",
                    justifyContent: "center",
                    alignSelf: "flex-end",
                    marginTop: 20
                }}>
                    <Text
                        onPress={() => alert("hello")}
                        style={{ color: "red" }}>Forgot Password?</Text>
                </View>
            </View >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 20,

    },
    input: {
        height: 40,
        backgroundColor: 'red',
        marginBottom: 20,
        color: 'black',
        paddingHorizontal: 10
    }
});



