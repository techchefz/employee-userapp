import React, { Component } from 'react';
import { StyleSheet, View, Image, Text, KeyboardAvoidingView, Dimensions } from 'react-native';
import LoginForm from '../screens/LoginForm';

export default class LogIn extends Component {
    render() {
        return (
            <KeyboardAvoidingView
                behavior="padding"
                enabled
                style={styles.container}>
                <View style={styles.logoContainer}>
                    <Image
                        style={styles.logo}
                        source={require('../images/logo.png')}
                    />
                </View>
                <View style={{justifyContent:"center",alignItems:"center",width:"100%"}}>
                    <LoginForm navigation={this.props.navigation} />
                </View>
                <View style={{
                    justifyContent: 'center',
                    flex:1,
                    alignItems: 'flex-end',
                    flexDirection: 'row',
                    marginBottom:26
                }}>
                    <Text>
                        Not Registered ?
                    </Text>
                    <Text style={{ color: '#008CD0' }}
                        onPress={() => { this.props.navigation.navigate('SignUP') }} >
                        SignUp
                    </Text>
                </View>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    logoContainer: {
        height: '50%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        height: Dimensions.get("screen").height * (1 / 3)
    },
    logo: {
        width: '50%',
        height: '50%',
        resizeMode: 'contain',
    },

});