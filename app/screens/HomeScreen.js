import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Header } from 'react-native-elements';

export default class HomeScreen extends React.Component {
  render() {
    return (
      <View>
        <Header
          placement="left"
          leftComponent={{ icon: 'menu', color: '#fff' , onPress: ()=>this.props.navigation.toggleDrawer() }}
          centerComponent={{ text: 'Home', style: { color: '#fff' } }}
        />
        <ScrollView>
          
        </ScrollView>
        </View>
    );
  }
}