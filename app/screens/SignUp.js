import React from 'react';
import { View, Text, KeyboardAvoidingView, StyleSheet, TextInput, Label, Button, ActivityIndicator, TouchableOpacity, Platform, SafeAreaView, StatusBar } from 'react-native';
import IP from '../ip';
import Moicon from 'react-native-vector-icons/Feather';

export default class SignUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            empID: '',
            password: '',
        }
    }

    signUp() {
        fetch('http://' + IP + '/signup', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: this.state.name.toLowerCase(),
                email: this.state.email.toLowerCase(),
                password: this.state.password,
                userType: 'employee',
                empID: this.state.empID.toLowerCase()
            }),
        })
            .then((res) => {
                if (res.status == 200) {
                    alert('Successfully Signed UP');
                    this.props.navigation.navigate('Login');
                } else {
                    alert('Something went wrong \n Try Again!')
                }
            });
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor="blue"
                    barStyle="light-content"
                />

                <SafeAreaView style={{ backgroundColor: "#008CD0" }}/>
                <View style={{
                    backgroundColor: "#008CD0",
                    flexDirection: "row",
                    height: 50,
                }}>
                    <View
                        style={{ flex: 1, height: "100%", justifyContent: "center",
                         alignItems: "center", backgroundColor: "#008CD0" }}>
                        <Moicon name='arrow-left'
                        onPress={()=>  this.props.navigation.goBack()}
                            color='#fff'
                            size={30}

                        />
                    </View>
                    <View style={{
                        width: "100%",
                        height: "100%",
                        flex: 2,
                        justifyContent: "center",
                        alignItems: "center"
                    }}>
                        <Text style={{ color: "#fff", fontSize: 20 }}>Register</Text>
                    </View>
                    <View style={{ flex: 1, height: "100%", backgroundColor: "#008CD0" }}></View>
                </View>

                <View style={{ padding: 20, justifyContent: "space-between" }}>

                    <View style={{ marginTop: '20%' }}>

                        <TextInput
                            placeholder='Name'
                            placeholderTextColor="#008CD0"
                            onChangeText={(uname) => this.setState({ name: uname })}
                            style={styles.input}
                        />

                        <TextInput
                            placeholder='Email'
                            placeholderTextColor="#008CD0"
                            onChangeText={(email) => { this.setState({ email: email }) }}
                            style={styles.input}
                        />

                        <TextInput
                            placeholder='Employee ID'
                            placeholderTextColor= "#008CD0"
                            onChangeText={(eid) => { this.setState({ empID: eid }) }}
                            style={styles.input}
                        />

                        <TextInput
                            placeholder='Password'
                            placeholderTextColor="#008CD0"
                            onChangeText={(password) => { this.setState({ password: password }) }}
                            secureTextEntry
                            style={styles.input}
                        />
                    </View>
                    <View style={{ justifyContent: "center", alignItems: "center" }}>
                        <TouchableOpacity
                            onPress={() => this.signUp()}
                            style={[styles.Button, {
                                backgroundColor:"#008CD0",
                                marginTop: 30,
                                justifyContent: "center",
                                width: "90%",
                                alignItems: "center",
                                borderRadius: 7,
                                padding: 10,
                                ...Platform.select({
                                    ios: {
                                        shadowColor: "black",
                                        shadowOffset: {
                                            width: 2,
                                            height: 2
                                        },
                                        shadowOpacity: 0.5,
                                        shadowRadius: 2,
                                    },
                                    android: { elevation: 3 }
                                })
                            }]}>
                            <Text style={{ color: "#fff" }}>SignUp</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: "100%"
    },
    input: {
        height: 40,
        marginTop: 10,
        width: "100%",
        borderColor: "gray",
        borderBottomWidth: 0.5,
        backgroundColor: 'transparent',
        marginBottom: 20,
        color: 'black',
        paddingHorizontal: 10
    }
});
