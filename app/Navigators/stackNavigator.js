import { createStackNavigator } from 'react-navigation';
import LogIn from '../screens/LogIn';
import UserHomeScreen from "../screens/HomeScreen";
import { MyDrawer } from '../Navigators/MyDrawer';
import SignUp from '../screens/SignUp';

export let StackNavigator = createStackNavigator({
    Login: {
      screen: LogIn,
    },
    Dashboard: {
      screen: MyDrawer,
    },
    SignUP: {
      screen: SignUp,
    },
  },{
    headerMode:'none'
  });
